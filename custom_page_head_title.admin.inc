<?php
/**
 * Displays the form for the standard settings tab.
 *
 * @return
 *   array A structured array for use with Forms API.
 */

function custom_page_head_title_admin_settings(){
	$edit_mode = FALSE;
	$page_head_title_id = 0;
	/* Check mode of page whether it is on editable mode */
	if(is_numeric(arg(3)) && (arg(4) == 'edit')){
		$edit_mode = TRUE;
		$result = db_fetch_object(db_query("SELECT * FROM {custom_page_head_title} WHERE id = %d",arg(3)));
		$page_head_title = $result->page_head_title;
		$page_url = $result->page_url;
		$button_type = 'Update';
		$page_head_title_id = arg(3);
	}
	$form['id'] = array('#type' => 'hidden', '#value' => $page_head_title_id);
	$form['custom_patterns'] = array(
		'#type' => 'fieldset',
		'#title' => t('Custom Page Head Title Patterns'),
		'#collapsible' => FALSE,
	);
	$form['custom_patterns']['page_head_title'] = array(
		'#title' => t('Page Title'),
		'#type' => 'textfield',
		'#required' => TRUE,
		'#default_value' => isset($edit_mode) ? $page_head_title : '',
		'#maxlength' => 70,
		'#description' => t('Provide a description of this page to appear in the &lt;title&gt; tag which search engines can use in search result listings (optional). It is generally accepted this field should be less than 70 characters.')
	);
	$form['custom_patterns']['page_url'] = array(
		'#title' => t('Page Url'),
		'#type' => 'textfield',
		'#required' => TRUE,
		'#default_value' => isset($edit_mode) ? $page_url : '',
	);
	$form['custom_patterns']['submit'] = array(
		'#value' => isset($edit_mode) ? t('Update') : t('Submit'),
		'#type' => 'submit',
	);
	$form['custom_patterns_list'] = array(
		'#type' => 'fieldset',
		'#title' => t('Custom Page Head Title List'),
		'#collapsible' => FALSE,
	);
	$form['custom_patterns_list']['list'] = array(
		'#value' => _custom_url_list(),
		'#type' => 'markup',
	);
	return $form;

}
/**
 * Implementation of hook_validate().
 */
function custom_page_head_title_admin_settings_validate($form,$form_state){
	$src = $form_state['values']['page_url'];
	$item = menu_get_item($src);
	if (!$item || !$item['access']) {
		form_set_error('src', t("The path '@link_path' is either invalid or you do not have access to it.", array('@link_path' => $src)));
	}
	if(db_result(db_query("SELECT 1 FROM {custom_page_head_title} WHERE page_url = '%s'", trim($form_state['values']['page_url']))) && !($form_state['values']['id'])){
		form_set_error('page_url', t('The url %url is already in use.', array('%url' => $form_state['values']['page_url'])));
	}
}
/**
 * Implementation of hook_submit().
 */
function custom_page_head_title_admin_settings_submit($form,$form_state){
	if($form_state['values']['id'] > 0){
		db_query("UPDATE {custom_page_head_title} SET page_head_title = '%s', page_url = '%s' WHERE id = %d",trim($form_state['values']['page_head_title']), trim($form_state['values']['page_url']),$form_state['values']['id']);
	}else{
		db_query("INSERT INTO {custom_page_head_title} (page_head_title,page_url) VALUES ('%s','%s')",trim($form_state['values']['page_head_title']), trim($form_state['values']['page_url']));
	}
}
function _custom_url_list(){
	$header = array('Custom Page Head Title', 'Custom Page Url', 'Operations');
	$rows = array();
	$sql = "SELECT * FROM {custom_page_head_title} ORDER BY id DESC";
	$sql_count = "SELECT count(*) FROM {custom_page_head_title}";
	$query = pager_query($sql, 20, 0, $sql_count);
	$has_record = FALSE;
	while ($result = db_fetch_object($query)) {
		$has_record = TRUE;
		$rows[] = array(
			$result->page_head_title,
			$result->page_url,
			l('Edit','admin/settings/custom-page-head-title/'.$result->id.'/edit',$options = array('query' => drupal_get_destination()))
			.' | '.l('Delete','admin/settings/custom-page-head-title/'.$result->id.'/delete',$options = array('query' => drupal_get_destination()))
		);
	}
	if($has_record){
		$output .= theme('table', $header, $rows);
		$output .= theme('pager', NULL, 20, 0);
	}else{
		$output = 'No record found.';
	}
	return $output;
}

/**
 * Menu callback; delete a single custom page url
 */
function custom_page_head_title_delete_confirm(&$form_state, $id) {
	$head_title = db_result(db_query("SELECT page_head_title FROM {custom_page_head_title} WHERE id = %d",$id));
	$form['head_title'] = array('#type' => 'value', '#value' => $head_title);
	$form['id'] = array('#type' => 'value', '#value' => $id);
	$message = t('Are you sure you want to delete the custom url " %title "?', array('%title' => $head_title));
	$caption = '';
	$caption .= '<p>'. t('This action cannot be undone.') .'</p>';
	return confirm_form($form, $message, 'admin/settings/custom-page-head-title', $caption, t('Delete'));
}
/**
 * Process custom page url delete confirm submissions.
 */
function custom_page_head_title_delete_confirm_submit($form, &$form_state) {
	db_query("DELETE FROM {custom_page_head_title} WHERE id = %d",$form_state['values']['id']);
	$head_title = array('%title' => $form_state['values']['head_title']);
	drupal_set_message(t('The custom page title "%title" has been deleted.', $head_title));
	$form_state['redirect'] = 'admin/settings/custom-page-head-title';
	return;
}
